package org.shraddha.request;

import org.springframework.stereotype.Component;

@Component
public class GroupRequest 
{
	private String groupName;
	private int groupId;
	private String type;
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public int getGroupId() 
	{
		return groupId;
	}
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
