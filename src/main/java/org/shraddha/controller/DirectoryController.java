package org.shraddha.controller;
import org.shraddha.request.LoginRequest;
import org.shraddha.request.SignUpRequest;
import org.shraddha.response.GetUserResponse;
import org.shraddha.response.LoginResponse;
import org.shraddha.response.SignUpResponse;
import org.shraddha.services.GetUserService;
import org.shraddha.services.LoginService;
import org.shraddha.services.SignUpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DirectoryController
{
	@Autowired
	SignUpService signUpService;
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	GetUserService getUserService;

	@PostMapping(path ="api/v1/signup",produces= {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE}, consumes= {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<SignUpResponse> signup(@RequestBody SignUpRequest request)
	{
		return signUpService.saveData(request);
	}
	
	@PostMapping(path ="api/v1/validate")
	public LoginResponse login(@RequestBody LoginRequest request) 
	{
		
		return loginService.login(request);
		
	}
	@GetMapping(path="directory/api/v1/users")
	public  GetUserResponse getUser()
	{
		 return getUserService.getUser();
	}
}
