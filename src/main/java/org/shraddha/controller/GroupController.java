package org.shraddha.controller;
import org.shraddha.request.GroupRequest;
import org.shraddha.response.GroupResponse;
import org.shraddha.services.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GroupController
{
	@Autowired
	GroupService groupService;
	
	@PostMapping(path ="api/v1/creategroup")
	public GroupResponse createFriend(@RequestBody GroupRequest request)
	{
		System.out.println(request.getGroupId());
		System.out.println(request.getGroupName());
		System.out.println(request.getType());
		GroupResponse lp = new GroupResponse();
		lp.setStatus("Success");
		lp.setMessage("Group Created Successfully");
		lp.setGroupId(request.getGroupId());
		lp.setGroupName(request.getGroupName());
       return lp;
	}
	
	@PostMapping(path="api/v1/friends")
	public GroupResponse mappGroup(@RequestBody GroupRequest request)
	{
		System.out.println(request.getGroupId());
		
		GroupResponse lp= new GroupResponse();
		lp.setGroupId(request.getGroupId());
	  
		return lp;
	}
}
