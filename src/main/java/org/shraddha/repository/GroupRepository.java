package org.shraddha.repository;

import org.shraddha.entity.GroupInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends JpaRepository<GroupInformation,Long>
{
	
	
}
