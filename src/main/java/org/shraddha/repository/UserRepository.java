package org.shraddha.repository;

import org.shraddha.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface UserRepository extends JpaRepository<User,Long>
{

	User findByFullName(String fullName);
	User findByEmail(String email);
	User findByMobile(String mobile);
	User findByPassword(String password);


}
