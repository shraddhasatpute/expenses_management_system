package org.shraddha.services;

import org.shraddha.entity.User;
import org.shraddha.repository.UserRepository;
import org.shraddha.request.SignUpRequest;
import org.shraddha.response.GroupResponse;
import org.shraddha.response.SignUpResponse;
import org.shraddha.response.UserData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
@Service
public class SignUpService 
{
	@Autowired
	UserRepository userRepository;
	@Autowired
	User user;
	@Autowired
	SignUpResponse response;
	
	/*req 1:No email and no Password present in db then create new user
	 * req 2:If email or Mobile is already present and password is empty then create new user(friend must have already create allow him to signup)
	 * req 3:If email or Mobile is already present and password also present then do not allow to create user
	 */
	public ResponseEntity<SignUpResponse> saveData(SignUpRequest request)
 {

	    response = new SignUpResponse();
	    response.setData(new UserData());
		//req:1
	    user = new User();
		//req:3
		if((null != userRepository.findByEmail(request.getEmail())&&null != userRepository.findByEmail(request.getEmail()).getPassword()))
				return getConflictSignUPResponse();
		//req:3
		if((null != userRepository.findByMobile(request.getMobile())&&null != userRepository.findByMobile(request.getMobile()).getPassword()))
			return getConflictSignUPResponse();
		//req :2 for mobile number is present but password is not set(friend creating using mobile)
		if((null != userRepository.findByMobile(request.getMobile())&&null == userRepository.findByMobile(request.getMobile()).getPassword()))
		{
			user=userRepository.findByMobile(request.getMobile());
		}
		
		//req :2 for email  is present but password is not set(friend creating using Email)
				if((null != userRepository.findByEmail(request.getEmail())&&null == userRepository.findByEmail(request.getEmail()).getPassword()))
				{
					user=userRepository.findByMobile(request.getMobile());
				}

		user.setCurrency(request.getCurrency());
		user.setCountry(request.getCountry());
		user.setFullName(request.getFullName());
		user.setEmail(request.getEmail());
		user.setLanguage(request.getLanguage());
		user.setMobile(request.getMobile());
		user.setPassword(request.getPassword());

		user = userRepository.save(user);
		response.setStatus("success");
		response.setMessage("User account created successfully");
		response.getData().setUserId(user.getUserId());
		response.getData().setCountry(user.getCountry());
		response.getData().setCurrency(user.getCurrency());
		response.getData().setEmail(user.getEmail());
		response.getData().setFullName(user.getFullName());
		response.getData().setLanguage(user.getLanguage());
		response.getData().setMobile(user.getMobile());

		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}
	public ResponseEntity<SignUpResponse> getConflictSignUPResponse()
	{
		SignUpResponse response=new SignUpResponse();
		response.setStatus("error");
		response.setMessage("Email or Mobile Number Already Regiosterd");
		response.setData(null);
		 return ResponseEntity.status(HttpStatus.CREATED).body(response);

	}
	
}
