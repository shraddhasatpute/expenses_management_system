package org.shraddha.services;

import org.shraddha.entity.GroupInformation;

import org.shraddha.repository.GroupRepository;

import org.shraddha.request.GroupRequest;
import org.shraddha.response.GroupResponse;
import org.shraddha.response.SignUpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
@Service
public class GroupService 
{
	
	@Autowired
	GroupResponse groupResponse;
	
	@Autowired
	GroupRepository groupRepository;
	
	@Autowired
	GroupInformation groupInformation;
	
	
	
	public void createGroup(GroupRequest request)
	{
		 groupResponse.setGroupId(request.getGroupId());
		 groupResponse.setGroupName(request.getGroupName());
		 groupResponse.setType(request.getType());
		 groupRepository.save(groupInformation);
	}
	  
}
