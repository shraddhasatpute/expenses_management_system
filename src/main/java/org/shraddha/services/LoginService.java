package org.shraddha.services;

import org.shraddha.entity.GroupInformation;
import org.shraddha.entity.User;
import org.shraddha.repository.GroupRepository;
import org.shraddha.repository.UserRepository;
import org.shraddha.request.LoginRequest;
import org.shraddha.response.GetUserResponse;
import org.shraddha.response.GroupResponse;
import org.shraddha.response.LoginResponse;
import org.shraddha.response.SignUpResponse;
import org.shraddha.response.UserFail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class LoginService 
{
	@Autowired
	UserRepository userRepository;
	@Autowired
	User user;
	@Autowired
	SignUpResponse response;
	@Autowired
	LoginResponse loginResponse;
	@Autowired
	LoginResponse loginResponseFails;
	@Autowired
	UserFail fails;
	
	public LoginResponse login(LoginRequest request) {
		if (null != userRepository.findByEmail(request.getEmail())
				&& null != userRepository.findByMobile(request.getMobile())
				&& null != userRepository.findByPassword(request.getPassword())) {


			user.setEmail(request.getEmail());
			user.setMobile(request.getMobile());
			user.setPassword(request.getPassword());

			user = userRepository.findByEmail(request.getEmail());
			user = userRepository.findByMobile(request.getMobile());
			user = userRepository.findByPassword(request.getPassword());
			loginResponse.setStatus("success");
			loginResponse.setMessage("Validation successfull");
			loginResponse.getData().setCountry(user.getCountry());
			loginResponse.getData().setCurrency(user.getCurrency());
			loginResponse.getData().setEmail(user.getEmail());
			loginResponse.getData().setFullName(user.getFullName());
			loginResponse.getData().setLanguage(user.getLanguage());
			loginResponse.getData().setMobile(user.getMobile());
			loginResponse.setErrors(null);

			return loginResponse;
			
			
		}
		loginResponseFails.setStatus("error");
		loginResponseFails.setMessage("Validation failed");
		loginResponseFails.setData(null);
	   
		loginResponseFails.getErrors().setField("email");
		loginResponseFails.getErrors().setMessage("Email is not valid");
		loginResponseFails.getErrors().setMessage("mobile");
		loginResponseFails.getErrors().setMessage("Mobile number is required");
		

		return loginResponseFails;
		
	}
}
