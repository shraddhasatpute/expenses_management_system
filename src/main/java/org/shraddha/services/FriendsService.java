package org.shraddha.services;
import org.shraddha.entity.User;
import org.shraddha.repository.UserRepository;
import org.shraddha.request.FriendsRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class FriendsService
{
	@Autowired
	UserRepository userRepository;
	@Autowired
	User user;
	
	public void createFriend(FriendsRequest request) 
	  {
		user.setFullName(request.getFullName());
		user.setEmail(request.getEmail());
		user.setMobile(request.getMobile());
		userRepository.save(user);
	}
	  
	 
	 
}
