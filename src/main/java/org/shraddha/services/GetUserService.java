package org.shraddha.services;

import java.util.ArrayList;
import java.util.List;

import org.shraddha.entity.User;
import org.shraddha.repository.UserRepository;
import org.shraddha.response.GetUserData;
import org.shraddha.response.GetUserResponse;
import org.shraddha.response.UserFail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GetUserService
{
	@Autowired
	UserRepository userRepository;
	@Autowired
	User user;
	@Autowired
	UserFail fails;
	@Autowired
	GetUserResponse gr;
	public GetUserResponse getUser()
	 {
		  List<User> userList = userRepository.findAll();
			List<GetUserData> getUserDataList = new ArrayList<>();

			for (User users : userList)
			{

				 GetUserData getUserData = new GetUserData();
				  
				 getUserData.setEmail(user.getEmail());
				 getUserData.setMobile(user.getMobile());
				 getUserData.setUserId(user.getUserId());
				 getUserData.setFullName(user.getFullName());
				 getUserData.setCurrency(user.getCurrency());
				 getUserData.setLanguage(user.getLanguage());
				 getUserDataList.add(getUserData);
			}
					
			gr.setStatus("Success");
			gr.setMessage("Data fetch successful");
			gr.setData(getUserDataList);

			return gr;
		 
		}
}
