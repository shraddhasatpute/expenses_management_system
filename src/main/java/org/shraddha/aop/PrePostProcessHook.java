package org.shraddha.aop;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.shraddha.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class PrePostProcessHook 
{
private static final Logger logger=LoggerFactory.getLogger(PrePostProcessHook .class);

@After("execution(* org.shraddha.repository.*.save(..))")
public void afterExecution(JoinPoint joinPoint)
{
	
System.out.println("\n*******Saved object -"+joinPoint.getArgs()[0]);

	User user=(User) joinPoint.getArgs()[0];
	System.out.println( user.getCountry());
	System.out.println(user.getCurrency());
	System.out.println(user.getEmail());
	System.out.println(user.getPassword());
}

@Before("execution(* org.shraddha.repository.*.save(..))")
public void beforeExecution(JoinPoint joinPoint)
{
	
System.out.println("\n*******Saving object -"+joinPoint.getArgs()[0]);

	User user=(User) joinPoint.getArgs()[0];
	System.out.println(user.getCountry());
	System.out.println(user.getCurrency());
	System.out.println(user.getEmail());
	System.out.println(user.getPassword());
	if(user.getPassword().length()<12)
	{
		System.out.println("Password is less than 12 character");
	}
}
	
	
}
