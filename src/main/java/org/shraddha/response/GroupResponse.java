package org.shraddha.response;

import org.springframework.stereotype.Component;

@Component
public class GroupResponse
{
	private String status;
	private String message;
	private int groupId;
	private String groupName;
	private String type;
	public int getGroupId()
	{
		return groupId;
	}
	public void setGroupId(int groupId) 
	{
		this.groupId = groupId;
	}
	
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
	public String getMessage() 
	{
		return message;
	}
	public void setMessage(String message) 
	{
		this.message = message;
	}
	
	public String getGroupName() 
	{
		return groupName;
	}
	public void setGroupName(String groupName)
	{
		this.groupName = groupName;
	}
	public String getType()
	{
		return type;
	}
	public void setType(String type) 
	{
		this.type = type;
	}

}
