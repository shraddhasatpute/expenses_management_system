package org.shraddha.response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LoginResponse 
{
	private String status;
	private String message;
	
	@Autowired
	private UserData data;
	
	@Autowired
	private UserFail errors;
	
	public UserFail getErrors() 
	{
		return errors;
	}	public void setErrors(UserFail errors)
	{
		this.errors = errors;
	}
	public String getStatus() {
		return status;
	}
	public UserData getData() {
		return data;
	}
	public void setData(UserData data) {
		this.data = data;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
